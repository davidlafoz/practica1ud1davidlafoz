package com.david.cosmeticos;

import java.time.LocalDate;

public class ProductosCosmeticos {
    /**
     * @author David Lafoz Feito
     * Atributos de la clase ProductosCosmeticos
     */
    private int id;
    private String nombre;
    private double precio;
    private String modelo;
    private double ph;
    private LocalDate fechaCaducidad;

    /**
     * Creado constructor vacio
     */
    public ProductosCosmeticos(){

    }

    /**
     *
     * @param id
     * @param nombre
     * @param precio
     * @param modelo
     * @param ph
     * @param fechaCaducidad
     * Creado constructor pasandole las variables
     */
    public ProductosCosmeticos(int id, String nombre, double precio, String modelo, double ph, LocalDate fechaCaducidad) {
        this.id=id;
        this.nombre = nombre;
        this.precio = precio;
        this.modelo = modelo;
        this.ph = ph;
        this.fechaCaducidad = fechaCaducidad;
    }

    /**
     * Crear los getters y setters
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getPh() {
        return ph;
    }

    public void setPh(double ph) {
        this.ph = ph;
    }

    public LocalDate getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(LocalDate fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
