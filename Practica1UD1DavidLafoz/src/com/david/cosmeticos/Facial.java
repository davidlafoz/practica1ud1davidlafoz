package com.david.cosmeticos;

import java.time.LocalDate;

public class Facial extends ProductosCosmeticos{

    private double pesoGramo;

    /**
     * Crear constructor con el contenido del campo padre
     */
    public Facial(){
        super();
    }

    /**
     * Crear constructor pasandole todo los parametros
     * @param id
     * @param nombre
     * @param precio
     * @param modelo
     * @param ph
     * @param fechaCaducidad
     * @param pesoGramo
     */

    public Facial(int id,String nombre, double precio, String modelo, double ph, LocalDate fechaCaducidad,double pesoGramo) {
        super(id,nombre, precio, modelo, ph, fechaCaducidad);
        this.pesoGramo=pesoGramo;
    }

    /**
     * Creado los getters y los setters
     * @return
     */
    public double getPesoGramo() {
        return pesoGramo;
    }

    public void setPesoGramo(double pesoGramo) {
        this.pesoGramo = pesoGramo;
    }

    /**
     * Creado el toString para mostrar los campos que queremos
     * @return
     */
    @Override
    public String toString() {
        return "Facial{" +
                " id= " + getId() +
                " nombre= " + getNombre() +
                " precio= " + getPrecio() +
                " modelo= " + getModelo() +
                " ph= " + getPh() +
                " pesoGramo= " + pesoGramo +
                '}';
    }
}
