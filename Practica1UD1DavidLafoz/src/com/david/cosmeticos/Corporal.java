package com.david.cosmeticos;

import java.time.LocalDate;

public class Corporal extends  ProductosCosmeticos {

    private int cantidad;

    /**
     * Crear constructor con el contenido del campo padre
     */
    public Corporal(){
        super();
    }

    /**
     * Crear constructor pasandole todo los parametros
     * @param id
     * @param nombre
     * @param precio
     * @param modelo
     * @param ph
     * @param fechaCaducidad
     * @param cantidad
     */
    public Corporal(int id,String nombre, double precio, String modelo, double ph, LocalDate fechaCaducidad,int cantidad) {
        super(id,nombre, precio, modelo, ph, fechaCaducidad);
        this.cantidad=cantidad;
    }

    /**
     * Creado los getters y setters
     * @return
     */
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Creado el toString para mostrar los campos que queremos
     * @return
     */
    @Override
    public String toString() {
        return " Corporal{ " +
                " id= " + getId() +
                " nombre= " + getNombre() +
                " precio= " + getPrecio() +
                " modelo= " + getModelo() +
                " ph= " + getPh() +
                " cantidad= " + cantidad +
                '}';
    }
}
