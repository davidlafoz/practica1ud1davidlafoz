package com.david;

import com.david.gui.CosmeticosControlador;
import com.david.gui.CosmeticosModelo;
import com.david.gui.Interfaz;

public class Main {
    public static void main(String[] args) {

        /**
         * Creamos 3 objetos uno de la clase interfaz otro de la clase CosmeticosModelo
         * y otra de la clase CosmeticoControlador para accerder a esas clases
         */
        Interfaz interfaz = new Interfaz();
        CosmeticosModelo modelo = new CosmeticosModelo();
        CosmeticosControlador controlador = new CosmeticosControlador(modelo,interfaz);
    }
}
