package com.david.gui;

import com.david.cosmeticos.Corporal;
import com.david.cosmeticos.Facial;
import com.david.cosmeticos.ProductosCosmeticos;
import com.david.util.Util;
import jdk.nashorn.internal.scripts.JO;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

/**
 * Esta clase hara que tenga funcionalidad todos los botones
 */
public class CosmeticosControlador implements ActionListener, ListSelectionListener,WindowListener{

    private Interfaz interfaz;
    private CosmeticosModelo cosmeticosModelo;
    private File ultimaRutaExportada;

    /**
     * Creamos un constructor al que le pasamos como parametro la interfaz y el modelo
     * @param cosmeticosModelo
     * @param interfaz
     */
    public CosmeticosControlador(CosmeticosModelo cosmeticosModelo, Interfaz interfaz){
        this.interfaz=interfaz;
        this.cosmeticosModelo=cosmeticosModelo;
//Llamamos al metodo cargar datos configuracion
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion " + e.getMessage());
        }
        //Le enviamos a los listener para que tenga funcionalidad al ser pulsado
        addActionerListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * Este metodo cargara todos los datos de configuracion en el archivo productos.conf
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties (  );
        configuracion.load ( new FileReader ( "productos.conf" ) );
        ultimaRutaExportada=new File ( configuracion.getProperty ( "ultimaRutaExportada" ) );
    }

    /**
     *Este metodo actualiza los datos del fichero le pasaremos como parametro el fichero
     * y lo igualamos a la variable anteriormente creada
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada=ultimaRutaExportada;
    }

    /**
     * Este metodo guardara los datos en el archivo productos.conf le enviara las propiedades que sera la ruta y
     * un comentario creado manualmente por el creador de la aplicacion
     * @throws IOException
     */
    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties ( );
        configuracion.setProperty ( "ultimaRutaExportada",ultimaRutaExportada.getAbsolutePath ());
        configuracion.store ( new PrintWriter( "productos.conf" ), "Datos de configuracion de los productos" );
    }

    /**
     * Este metodo actualizara la lista de elementos en la interfaz
     * se borraran todos los elementos y los volvera añadir con el foreach
     */
    public void actualizar(){
        interfaz.dlmCosmeticos.clear();
        for(ProductosCosmeticos p : cosmeticosModelo.obtenerProductos()){
            interfaz.dlmCosmeticos.addElement(p);
        }
    }

    /**
     * Este metodo pondra todos los campos vacion en la interfaz grafica
     */
    public void limpiarCampos(){
        interfaz.idTxt.setText(null);
        interfaz.nombreTxt.setText(null);
        interfaz.precioTxt.setText(null);
        interfaz.modeloTxt.setText(null);
        interfaz.phTxt.setText(null);
        interfaz.pesoCantidadTxt.setText(null);
        interfaz.fechaCaducidadDPicker.setText(null);
        interfaz.idTxt.requestFocus();

    }

    /**
     * Este metodo dara funcionalidad a los botones añadir,importar,exportar y cambiara el nombre de los JLabel de pesogramo/cantidad
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String opcion = e.getActionCommand();
        switch (opcion){
            //Añadira todos los campos de la interfaz al arraylist
            case "Anadir":

                //comprueba que si hay campos vacios saldra un mensaje que avisa que no puede haber ningun campo vacio
                if(camposVacios()){
                    Util.mensajeError("Se tiene que rellenar todos los campos. No puede estar ningun campo vacio");
                    break;
                }
                //Comprueba si existe el producto y si existe saldra un mensaje en que avisa que ya existe el producto
                if(cosmeticosModelo.existeProducto(Integer.parseInt(interfaz.idTxt.getText()))){
                    Util.mensajeError("Ya existe ese producto " + interfaz.idTxt.getText());
                    break;
                }
                //Si has salecciinado el radio button facial llamaremos al metodo altafacial para añadirlo
                if(interfaz.facialRadioButton.isSelected()){
                    cosmeticosModelo.altaFacial(Integer.parseInt(interfaz.idTxt.getText()),
                            interfaz.nombreTxt.getText(),
                            Double.parseDouble(interfaz.precioTxt.getText()),
                            interfaz.modeloTxt.getText(),
                            Double.parseDouble(interfaz.phTxt.getText()),
                            interfaz.fechaCaducidadDPicker.getDate(),
                            Double.parseDouble(interfaz.pesoCantidadTxt.getText()));
                //En caso contrario se llamara al metodo altaCorporal para añadirlo
                }else{
                    cosmeticosModelo.altaCorporal(Integer.parseInt(interfaz.idTxt.getText()),
                            interfaz.nombreTxt.getText(),
                            Double.parseDouble(interfaz.precioTxt.getText()),
                            interfaz.modeloTxt.getText(),
                            Double.parseDouble(interfaz.phTxt.getText()),
                            interfaz.fechaCaducidadDPicker.getDate(),
                            Integer.parseInt(interfaz.pesoCantidadTxt.getText()));
                }
                //Limpiamos los campos de la interfaz
                limpiarCampos();
                //llamamos al metodo actualizar que actualiza la lista de la interfaz
                actualizar();
                break;
                //Cuando pulse importar podremos importar el arhivo xml que queramos
            case "Importar":
                //Llamamos al metodo crearSelectorFichero al que le enviamos  el tipo de archivo la extension y la ruta en la que exporta
                JFileChooser selectorFichero = Util.crearSelectorFichero(ultimaRutaExportada, "Archivos XML","xml");
                //creamos la variable a la que igualamaos selectorFichero que abrira una ventana
                int opt=selectorFichero.showOpenDialog(null);
                //si la opcion es verdadera importara el fichero seleccionado
                if(opt==JFileChooser.APPROVE_OPTION){
                    try {
                        cosmeticosModelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    //actualizamos el JList
                    actualizar();
                }
                break;
                //Si damos al boton exportar entrara en esta opcion
            case "Exportar":
                //Crearemos el selectorFichero2 al que igualamos a la llamada del metodo crearSelectorFichero
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                //Creamos una variable a la que igualamos al selectorfichero2 que guardara lo que habremos exportado
                int op2=selectorFichero2.showSaveDialog(null);
                //si la condicion es verdadera llamaremos al metodo exportarXML
                if(op2==JFileChooser.APPROVE_OPTION){
                    try {
                        cosmeticosModelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
                //Si marcamos facial la label pesoCantidad cambiara por Peso Gramo
            case "Facial":
                interfaz.pesoCantidadLbl.setText("Peso Gramo");
                break;
                //Si marcamos Corporal la label pesoCantidad cambiara por cantidad
            case "Corporal":
                interfaz.pesoCantidadLbl.setText("Cantidad");
                break;
        }
    }


    /**
     * Este metodo retornara si es verdadero o false, si retorna verdadero significa que algun campo esta vacio o no rellenado
     * Si retorna falso significa que estan todos los campos rellenados
     * @return
     */
    public boolean camposVacios(){
        if(interfaz.idTxt.getText().isEmpty() || interfaz.nombreTxt.getText().isEmpty() || interfaz.precioTxt.getText().isEmpty() || interfaz.modeloTxt.getText().isEmpty() ||
           interfaz.phTxt.getText().isEmpty() || interfaz.pesoCantidadTxt.getText().isEmpty() || interfaz.fechaCaducidadDPicker.getText().isEmpty()){
            return true;
        }
        return false;
    }

    /**
     * Este metodo mostrara una ventana para preguntar si desea salir de la aplicacion
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        //Creamos el mensaje que mostrara por pantalla
        int resp = Util.mensajeSalir("¿Desea salir de la aplicacion","Salir de la aplicacion");
        //Si la respuesta es si guardara los datos y saldra de la aplicacion
        if(resp== JOptionPane.YES_OPTION){

            try {
                guardarDatosConfiguracion();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);
        //En caso contrario no saldra de la aplicacion
        }if(resp==JOptionPane.NO_OPTION){

        }
    }

    /**
     * Este metodo realizara la opcion en la que cuando pinches el valor de la lista se pongan todos los datos
     * en las cajas de texto de la interfaz
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Si la condicion es verdadera le enviamos a los campos de texto de la interfaz los datos de la lista seleccionada
        if(e.getValueIsAdjusting()){
            ProductosCosmeticos productos = (ProductosCosmeticos) interfaz.list1.getSelectedValue();
            interfaz.idTxt.setText(String.valueOf(productos.getId()));
            interfaz.nombreTxt.setText(productos.getNombre());
            interfaz.precioTxt.setText(String.valueOf(productos.getPrecio()));
            interfaz.modeloTxt.setText(productos.getModelo());
            interfaz.phTxt.setText(String.valueOf(productos.getPh()));
            if(productos instanceof Facial){
                interfaz.facialRadioButton.doClick();
                interfaz.pesoCantidadTxt.setText(String.valueOf(((Facial) productos).getPesoGramo()));
            }else{
                interfaz.corporalRadioButton.doClick();
                interfaz.pesoCantidadTxt.setText(String.valueOf(((Corporal)productos).getCantidad()));
            }
            interfaz.fechaCaducidadDPicker.setDate(productos.getFechaCaducidad());

        }
    }

    /**
     * Este metodo dara funcionalidad a los botones
     * @param listener
     */
    public void addActionerListener(ActionListener listener){
        interfaz.facialRadioButton.addActionListener(listener);
        interfaz.corporalRadioButton.addActionListener(listener);
        interfaz.importarBtn.addActionListener(listener);
        interfaz.exportarBtn.addActionListener(listener);
        interfaz.nuevoBtn.addActionListener(listener);
    }

    /**
     * Dara funcionalidad al frame para que funcione
     * @param listener
     */
    public void addWindowListener(WindowListener listener){
        interfaz.frame.addWindowListener(listener);
    }

    /**
     * Este metodo dara funcionalidad a la lista para que funcione
     * @param listener
     */
    public void addListSelectionListener(ListSelectionListener listener){
        interfaz.list1.addListSelectionListener(listener);
    }
    @Override
    public void windowOpened(WindowEvent e) {

    }



    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {


    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
