package com.david.gui;

import com.david.cosmeticos.Corporal;
import com.david.cosmeticos.Facial;
import com.david.cosmeticos.ProductosCosmeticos;
import javafx.util.BuilderFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class CosmeticosModelo {

    private ArrayList<ProductosCosmeticos> listaProductos;

    /**
     * Creamos un constructor al que no le pasamos nada, creamos el arraylist dentro del constructor
     */
    public CosmeticosModelo() {
        this.listaProductos = new ArrayList<>();
    }

    /**
     * este metodo nos devolvera el arraylist
     * @return
     */
    public ArrayList<ProductosCosmeticos> obtenerProductos() {
        return listaProductos;
    }

    //CREACION DE PRODUCTOS FACIALES

    /**
     * Este metodo sirve para añadir un producto facial a la lista de productos cosmeticos
     * @param id
     * @param nombre
     * @param precio
     * @param modelo
     * @param ph
     * @param fechaCaducidad
     * @param pesoGramo
     */
    public void altaFacial(int id, String nombre, double precio, String modelo, double ph, LocalDate fechaCaducidad, double pesoGramo) {
        Facial nuevoFacial = new Facial(id, nombre, precio, modelo, ph, fechaCaducidad, pesoGramo);
        listaProductos.add(nuevoFacial);

    }

    //CREACION DE PRODUCTOS CORPORALES

    /**
     * Este metodo sirve para añadir un producto corporal a la lista de productos cosmeticos
     * @param id
     * @param nombre
     * @param precio
     * @param modelo
     * @param ph
     * @param fechaCaducidad
     * @param cantidad
     */
    public void altaCorporal(int id, String nombre, double precio, String modelo, double ph, LocalDate fechaCaducidad, int cantidad) {
        Corporal nuevoCorporal = new Corporal(id, nombre, precio, modelo, ph, fechaCaducidad, cantidad);
        listaProductos.add(nuevoCorporal);

    }

    //COMPROBAR PRODUCTOS POR ID

    /**
     * Este metodo sirve para saber si se repite el campo clave de un producto es decir para que no se repitan los productos
     * @param id
     * @return
     */
    public boolean existeProducto(int id) {
        for (ProductosCosmeticos p : listaProductos) {
            if (p.getId() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Este metodo sirve para crear el fichero xml de los productos que hemos añadido
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        //Creara la instancia del documento builder factory

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //Creamos un nuevo documento builder

        DocumentBuilder builder = factory.newDocumentBuilder();

         //Obtendra la implementacion del builder

        DOMImplementation dom = builder.getDOMImplementation();

         // Creamos el documento que sera xml

        Document documento = dom.createDocument(null, "xml", null);


         //* Creamos la etiqueta productos

        Element raiz = documento.createElement("Productos");

         //y le decimos que coja el elemento del docuemnto y se lo ponga como hijo a xml

        documento.getDocumentElement().appendChild(raiz);

        Element nodoProductos = null;
        Element nodoDatos = null;
        Text texto = null;


         //Hacemos un Foreach para que recorra el arraylist

        for (ProductosCosmeticos p : listaProductos) {

            //Comprobamos que si hemos elegido Facial o Corporal
            if (p instanceof Facial) {
                //Si hemos elegido facial creara la etiqueta Facial
                nodoProductos = documento.createElement("Facial");
            } else {
                //Si esa condicion es false pues creara la etiqueta Corporal
                nodoProductos = documento.createElement("Corporal");
            }
            //Añade la etiqueta anteriormente creada como hijo a raiz
            raiz.appendChild(nodoProductos);

            //Creamos la etiqueta id
            nodoDatos = documento.createElement("Id");
            //Y se añade como hijo a la etiqueta facial o corporal
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta id
            texto = documento.createTextNode(String.valueOf(p.getId()));
            //Le añadimos el texto a la etiqueta id
            nodoDatos.appendChild(texto);

            //Creamos la etiqueta nombre
            nodoDatos = documento.createElement("Nombre");
            //Y se añade como hijo a la etiqueta facial o corporal
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta nombre
            texto = documento.createTextNode(p.getNombre());
            //Le añadimos el texto a la etiqueta nombre
            nodoDatos.appendChild(texto);

            //Creamos la etiqueta precio
            nodoDatos = documento.createElement("Precio");
            //Y se añade como hijo a la etiqueta facial o corporal
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta precio
            texto = documento.createTextNode(String.valueOf(p.getPrecio()));
            //Le añadimos el texto a la etiqueta precio
            nodoDatos.appendChild(texto);

            //Creamos la etiqueta modelo
            nodoDatos = documento.createElement("Modelo");
            //Y se añade como hijo a la etiqueta facial o corporal
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta modelo
            texto = documento.createTextNode(p.getModelo());
            //Le añadimos el texto a la etiqueta modelo
            nodoDatos.appendChild(texto);

            //Creamos la etiqueta ph
            nodoDatos = documento.createElement("Ph");
            //Y se añade como hijo a la etiqueta facial o corporal
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta ph
            texto = documento.createTextNode(String.valueOf(p.getPh()));
            //Le añadimos el texto a la etiqueta ph
            nodoDatos.appendChild(texto);

            //Comprobamos que si hemos elegido facial
            if (p instanceof Facial) {
                //Creara la etiqueta peso-gramo
                nodoDatos = documento.createElement("Peso-Gramo");
                //Y se añade como hijo a la etiqueta facial
                nodoProductos.appendChild(nodoDatos);
                //Creamos el texto que va a llevar la etiqueta peso-gramo
                texto = documento.createTextNode(String.valueOf(((Facial) p).getPesoGramo()));
                //Le añadimos el texto a la etiqueta peso-gramo
                nodoDatos.appendChild(texto);
                //si la condicion es falsa
            } else {
                //Creara la etiqueta cantidad
                nodoDatos = documento.createElement("Cantidad");
                //Y se añade como hijo a la etiqueta corporal
                nodoProductos.appendChild(nodoDatos);
                //Creamos el texto que va a llevar la etiqueta cantidad
                texto = documento.createTextNode(String.valueOf(((Corporal) p).getCantidad()));
                //Le añadimos el texto a la etiqueta cantidad
                nodoDatos.appendChild(texto);
            }

            //Creara la etiqueta fecha-caducidad
            nodoDatos = documento.createElement("Fecha-caducidad");
            //Y se añade como hijo a la etiqueta corporal o facial
            nodoProductos.appendChild(nodoDatos);
            //Creamos el texto que va a llevar la etiqueta fecha-caducidad
            texto = documento.createTextNode(p.getFechaCaducidad().toString());
            //Le añadimos el texto a la etiqueta fecha-caducidad
            nodoDatos.appendChild(texto);

            //Guardaremos los datos en el fichero
            Source source = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            // los transformamos
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        }

    }

    /**
     * Este metodo sirve para importar los ficheros xml es decir veremos el archivo xml en la aplicacion grafica
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaProductos = new ArrayList<>();
        Facial nuevaFacial = null;
        Corporal nuevoCorporal = null;
        //creamos la instancia
        // cremos el nuevo documento con esa instancia
        //creamos el documento en el parseamos el fichero

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        //Creamos una lista de elementos que cogera todas las etiquetas
        NodeList listaElementos = documento.getElementsByTagName("*");
        for (int i = 0; i < listaElementos.getLength(); i++) {
            //Le asignamos al nodoCosmetico la lista de elementos de i
            Element nodoCosmetico = (Element) listaElementos.item(i);

            if (nodoCosmetico.getTagName().equals("Facial")) {
                nuevaFacial = new Facial();
                //Le enviamos al campo que queremos el contenido que queremos enviarle
                nuevaFacial.setId(Integer.parseInt(nodoCosmetico.getChildNodes().item(0).getTextContent()));
                nuevaFacial.setNombre(nodoCosmetico.getChildNodes().item(1).getTextContent());
                nuevaFacial.setPrecio(Double.parseDouble(nodoCosmetico.getChildNodes().item(2).getTextContent()));
                nuevaFacial.setModelo(nodoCosmetico.getChildNodes().item(3).getTextContent());
                nuevaFacial.setPh(Double.parseDouble(nodoCosmetico.getChildNodes().item(4).getTextContent()));
                nuevaFacial.setPesoGramo(Double.parseDouble(nodoCosmetico.getChildNodes().item(5).getTextContent()));
                nuevaFacial.setFechaCaducidad(LocalDate.parse(nodoCosmetico.getChildNodes().item(6).getTextContent()));
                listaProductos.add(nuevaFacial);
            } else {
                if (nodoCosmetico.getTagName().equals("Corporal")) {
                    nuevoCorporal = new Corporal();
                    //Le enviamos al campo que queremos el contenido que queremos enviarle
                    nuevoCorporal.setId(Integer.parseInt(nodoCosmetico.getChildNodes().item(0).getTextContent()));
                    nuevoCorporal.setNombre(nodoCosmetico.getChildNodes().item(1).getTextContent());
                    nuevoCorporal.setPrecio(Double.parseDouble(nodoCosmetico.getChildNodes().item(2).getTextContent()));
                    nuevoCorporal.setModelo(nodoCosmetico.getChildNodes().item(3).getTextContent());
                    nuevoCorporal.setPh(Double.parseDouble(nodoCosmetico.getChildNodes().item(4).getTextContent()));
                    nuevoCorporal.setCantidad(Integer.parseInt(nodoCosmetico.getChildNodes().item(5).getTextContent()));
                    nuevoCorporal.setFechaCaducidad(LocalDate.parse(nodoCosmetico.getChildNodes().item(6).getTextContent()));
                    listaProductos.add(nuevoCorporal);
                }
            }
        }
    }


}
