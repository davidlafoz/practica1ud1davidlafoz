package com.david.gui;

import com.david.cosmeticos.ProductosCosmeticos;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Interfaz {

    /**
     * Creado todas las variables de la interfaz
     */
    public JRadioButton facialRadioButton;
    public JRadioButton corporalRadioButton;
    public JTextField nombreTxt;
    public JTextField precioTxt;
    public JTextField modeloTxt;
    public JTextField phTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public DatePicker fechaCaducidadDPicker;
    public JPanel panel;
    public JTextField pesoCantidadTxt;
    public JLabel pesoCantidadLbl;
    public JTextField idTxt;
    public JFrame frame;

    public DefaultListModel<ProductosCosmeticos> dlmCosmeticos;

    /**
     * Creamos un constructir al que no le pasamos nada como parametro
     */
    public Interfaz() {
        /**
         * Ejecutara la interfaz para que se vea por ventana
         */
        frame = new JFrame("Productos Cosmeticos");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setBounds(500,100,700,700);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    /**
     * Metodo para pasarle todos los campos a la lista
     */
    public void initComponents(){
        /**
         * le enviamos los datos del arraylist al JList
         */
        dlmCosmeticos=new DefaultListModel<>();
        list1.setModel(dlmCosmeticos);
    }

}
