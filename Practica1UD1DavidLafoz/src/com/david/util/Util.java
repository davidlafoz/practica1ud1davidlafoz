package com.david.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Created by DAM on 05/11/2021.
 */
public class Util {
    /**
     * Este metodo mostrara un mensaje que le enviamos como parametro que saldra en la pantalla cuando haya un error
     * @param mensaje
     */
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Este metodo mostrara una ventana emergente en el que te preguntara si desea salir o no
     * @param mensaje
     * @param titulo
     * @return
     */
    public static int mensajeSalir(String mensaje, String titulo){
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * Este metodo permite decidir la ruta en la que sea guardar el archivo el tipo de archivo y la extension
     * @param rutaDefecto
     * @param tipoArchivos
     * @param extension
     * @return
     */
    public static JFileChooser crearSelectorFichero(File rutaDefecto, String tipoArchivos, String extension){
      JFileChooser selectorArchivo = new JFileChooser();
      //Si el fichero no esta vacio
      if(rutaDefecto!=null){
          //le enviamos la ruta del directorio
          selectorArchivo.setCurrentDirectory(rutaDefecto);
      }
      //Si la extension no esta vacia
      if(extension!=null){
          //Creamos el tipo de archivo y la extension
          FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
          //se lo enviamos a filtro
          selectorArchivo.setFileFilter(filtro);
      }
      //retornamos la ventana selectorArchivo
      return  selectorArchivo;
    }
}
